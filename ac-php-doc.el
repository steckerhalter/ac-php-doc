;;; ac-php-doc.el --- An auto-complete source using php completions
;;
;;; Author: Steckerhalter
;;; URL: https://github.com/steckerhalter/ac-php-doc
;;; Version: DEV
;;
;;; Commentary:
;; Usage:
;;     (setq php-manual-path "/usr/share/doc/php-doc/html/")
;;     (require 'ac-php-doc)
;;     (add-hook 'php-mode-hook 'set-up-php-ac)
;;
;;; Requirements:
;; - html2text command line program must be installed and available
;; - php manual must be available
;;
;;; Code:

(defun ac-php-doc-documentation (symbol-name)
  (let ((symbol-name (substring-no-properties symbol-name)) file doc)
    (setq file (expand-file-name (format "function.%s.html" (replace-regexp-in-string "_" "-" symbol-name)) php-manual-path))
    (when (file-readable-p file)
      (with-temp-buffer
        (insert (shell-command-to-string (concat "html2text -ascii -width 60 -style compact " file)))
        (beginning-of-buffer)
        (search-forward "Description")
        (forward-line)
        (beginning-of-line)
        (delete-region (point) (point-min))
        (buffer-string)
        ))))

;;;###autoload
(defface ac-php-doc-menu-face
  '((t (:inherit ac-candidate-face)))
  "Face for php candidate menu."
  :group 'auto-complete)

;;;###autoload
(defface ac-php-doc-selection-face
  '((t (:inherit ac-selection-face)))
  "Face for the php selected candidate."
  :group 'auto-complete)

;;;###autoload
(defvar ac-source-php-doc
  '((candidates . ac-buffer-dictionary)
    (candidate-face . ac-php-doc-menu-face)
    (selection-face . ac-php-doc-selection-face)
    (symbol . "p")
    (document . ac-php-doc-documentation)
    )
  "Auto completion source for PHP with quick help documentation")

;;;###autoload
(defun set-up-php-ac ()
  "Add an php quick help documentation source to the front of
`ac-sources' for the current buffer."
  (interactive)
  (if (executable-find "html2text")
      (if (boundp 'php-manual-path)
          (add-to-list 'ac-sources 'ac-source-php-doc)
        (message "php-manual-path not set. ac-php-doc needs the php manual to work."))
    (message "html2text command was not found. It is needed to use ac-php-doc. ac-php-doc was not enabled.")))


(provide 'ac-php-doc)
;;; ac-php-doc.el ends here
